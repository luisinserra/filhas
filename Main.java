import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    private static final int _0 = 0;

    public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {

        Object filha = null;
        Pai fi = new Pai();
        if (args[1].equals("2")) {
            filha = new Filha2();
            fi = new Filha2();
        } else if (args[1].equals("1")) {
            filha = new Filha1();
            fi = new Filha1();
        }
        // Method metodo = filha.getClass().getMethod("quemSou", String.class);
        if (args[1].equals("1") || args[1].equals("2")) {
            Method metodo = filha.getClass().getMethod(args[0]);
            String resp = (String) metodo.invoke(filha);
            System.out.println(resp);
        }
        System.out.println("Eu sou " + fi.quemSou());
        System.out.println("Meu pai é " + fi.meuPai());

/*
        StringCorte sc = new StringCorte();
        String ret = sc.cortaString();
*/        
    }
}
